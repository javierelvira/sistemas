import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.DropMode;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.ActionListener;
import javax.swing.JScrollPane;

public class ElviraLopez {

	private JFrame frame;
	private JPasswordField password;
	private JTextField usuario;
	private JRadioButton mas18;
	private JRadioButton terminosSi;
	private JRadioButton terminosNo;
	private JCheckBox terminos;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextPane txtpnTrminosYCondiciones;
	private JButton registrar;
	private JScrollPane scrollPane;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ElviraLopez window = new ElviraLopez();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ElviraLopez() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 650, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUsuario.setBounds(36, 69, 91, 14);
		frame.getContentPane().add(lblUsuario);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setBounds(36, 93, 91, 14);
		frame.getContentPane().add(lblPassword);
		
		password = new JPasswordField();
		password.setBounds(147, 91, 117, 17);
		frame.getContentPane().add(password);
		
		
		usuario = new JTextField();
		usuario.setBounds(147, 66, 118, 20);
		frame.getContentPane().add(usuario);
		usuario.setColumns(10);
		
		JRadioButton menos18 = new JRadioButton("Menor de 18 a\u00F1os");
		menos18.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				terminos.setEnabled(false);
			}
		});
		buttonGroup.add(menos18);
		menos18.setBounds(147, 129, 147, 23);
		frame.getContentPane().add(menos18);
		
		mas18 = new JRadioButton("A partir de 18 a\u00F1os");
		
		mas18.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				terminos.setEnabled(true);
			}
		});
		buttonGroup.add(mas18);
		mas18.setBounds(147, 155, 147, 23);
		frame.getContentPane().add(mas18);
		
		terminosSi = new JRadioButton("Ver t\u00E9rminos");
		terminosSi.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				txtpnTrminosYCondiciones.setVisible(true);
			}
		});		
		buttonGroup_1.add(terminosSi);
		terminosSi.setBounds(316, 129, 158, 23);
		frame.getContentPane().add(terminosSi);
		
		terminosNo = new JRadioButton("Ocultar t\u00E9rminos");
		terminosNo.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				txtpnTrminosYCondiciones.setVisible(false);
			}
		});	
		buttonGroup_1.add(terminosNo);
		terminosNo.setBounds(316, 155, 158, 23);
		frame.getContentPane().add(terminosNo);
		
		terminos = new JCheckBox("Acepto los t\u00E9rminos");
		terminos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				registrar.setEnabled(true);
			}
		});

		terminos.setEnabled(false);
		terminos.setBounds(147, 196, 147, 23);
		frame.getContentPane().add(terminos);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(316, 199, 194, 143);
		frame.getContentPane().add(scrollPane);
		
		txtpnTrminosYCondiciones = new JTextPane();
		scrollPane.setViewportView(txtpnTrminosYCondiciones);
		txtpnTrminosYCondiciones.setVisible(false);
		txtpnTrminosYCondiciones.setEditable(false);
		txtpnTrminosYCondiciones.setText("T\u00E9rminos y Condiciones del Contrato: Las presentes Condiciones Generales de contrataci\u00F3n, junto con las Condiciones Particulares que, en cada caso y servicio, puedan establecerse (en lo sucesivo, y en conjunto, las \"Condiciones de Contratacion\") regulan expresamente las relaciones surgidas entre ambas partes.");
		
		registrar = new JButton("Registrar Usuario");
		registrar.setEnabled(false);
		registrar.setBounds(147, 319, 147, 23);
		frame.getContentPane().add(registrar);
		registrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.out.println("Registro incorrecto, datos incompletos");
			}
		});
		/*registrar.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					if(//todo ok joseluis) {
						System.out.println("Registro correcto");
					}
					else {
						System.out.println("Registro incorrecto, datos incompletos");
					}
				}
			});
		*/
		
	}
}
