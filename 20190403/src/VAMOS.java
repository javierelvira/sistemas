import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextPane;

public class VAMOS {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VAMOS window = new VAMOS();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VAMOS() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 736, 498);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblInterfaz = new JLabel("Nombre: ");
		lblInterfaz.setBounds(48, 47, 83, 14);
		frame.getContentPane().add(lblInterfaz);
		
		textField = new JTextField();
		textField.setBounds(141, 44, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Hombre");
		rdbtnNewRadioButton.setBounds(141, 113, 109, 23);
		frame.getContentPane().add(rdbtnNewRadioButton);
		
		JLabel lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setBounds(48, 78, 83, 14);
		frame.getContentPane().add(lblApellidos);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(141, 75, 140, 20);
		frame.getContentPane().add(textField_1);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setBounds(48, 117, 83, 14);
		frame.getContentPane().add(lblSexo);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setBounds(266, 113, 109, 23);
		frame.getContentPane().add(rdbtnMujer);
		
		JLabel lblNombreDeUsuario = new JLabel("Usuario:");
		lblNombreDeUsuario.setBounds(48, 152, 83, 14);
		frame.getContentPane().add(lblNombreDeUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		lblContrasea.setBounds(48, 189, 83, 14);
		frame.getContentPane().add(lblContrasea);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(141, 149, 86, 20);
		frame.getContentPane().add(textField_2);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(141, 186, 86, 20);
		frame.getContentPane().add(passwordField);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Estudiante", "Profesor"}));
		comboBox.setBounds(141, 222, 108, 20);
		frame.getContentPane().add(comboBox);
		
		JLabel lblOcupacin = new JLabel("Ocupaci\u00F3n:");
		lblOcupacin.setBounds(48, 225, 83, 14);
		frame.getContentPane().add(lblOcupacin);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(286, 370, 89, 23);
		frame.getContentPane().add(btnEnviar);
		
		JLabel lblObjeciones = new JLabel("Objeciones:");
		lblObjeciones.setBounds(48, 276, 83, 14);
		frame.getContentPane().add(lblObjeciones);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(141, 276, 348, 79);
		frame.getContentPane().add(textPane);
	}
}
