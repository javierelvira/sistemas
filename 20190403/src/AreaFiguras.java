import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;

public class AreaFiguras {

	private JFrame frame;
	private JTextField radio;
	private JTextField lado;
	private JTextField base;
	private JTextField altura;
	private JTextField diagonalMayor;
	private JTextField diagonalMenor;
	private JTextField baseMayor;
	private JTextField baseMenor;
	private JTextField resultado;
	private JComboBox figura;
	private JButton hacer;
	private JLabel lblNewLabel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AreaFiguras window = new AreaFiguras();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AreaFiguras() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 800, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		figura = new JComboBox();
		figura.setModel(new DefaultComboBoxModel(new String[] {"Circulo", "Cuadrado", "Rectangulo", "Rombo", "Trapecio", "Triangulo"}));
		figura.setToolTipText("");
		figura.setBounds(348, 71, 167, 30);
		frame.getContentPane().add(figura);
		
		JLabel lblSeleccioneLaFigura = new JLabel("Seleccione la figura para calcular su area:");
		lblSeleccioneLaFigura.setBounds(50, 71, 261, 30);
		frame.getContentPane().add(lblSeleccioneLaFigura);
		
		radio = new JTextField();
		radio.setText("0");
		radio.setBounds(324, 131, 86, 20);
		frame.getContentPane().add(radio);
		radio.setColumns(10);
		
		JLabel lblLado = new JLabel("Lado: (Cuadrado)");
		lblLado.setBounds(50, 162, 246, 20);
		frame.getContentPane().add(lblLado);
		
		JLabel lblRadio = new JLabel("Radio: (C\u00EDrculo)");
		lblRadio.setBounds(50, 131, 246, 20);
		frame.getContentPane().add(lblRadio);
		
		JLabel lblBase = new JLabel("Base: (Rect\u00E1ngulo, Tri\u00E1ngulo)");
		lblBase.setBounds(50, 198, 246, 20);
		frame.getContentPane().add(lblBase);
		
		JLabel lblAltura = new JLabel("Altura: (Rect\u00E1ngulo, Tri\u00E1ngulo, Trapecio)");
		lblAltura.setBounds(50, 229, 246, 20);
		frame.getContentPane().add(lblAltura);
		
		JLabel lblRombo = new JLabel("Diagonal Mayor: (Rombo)\r\n");
		lblRombo.setBounds(50, 261, 246, 20);
		frame.getContentPane().add(lblRombo);
		
		JLabel lblDiagonalMenor = new JLabel("Diagonal Menor: (Rombo)");
		lblDiagonalMenor.setBounds(50, 292, 246, 20);
		frame.getContentPane().add(lblDiagonalMenor);
		
		JLabel lblBaseMayor = new JLabel("Base Mayor: (Trapecio)");
		lblBaseMayor.setBounds(50, 323, 246, 20);
		frame.getContentPane().add(lblBaseMayor);
		
		JLabel lblBaseMenor = new JLabel("Base Menor: (Trapecio)");
		lblBaseMenor.setBounds(50, 354, 246, 20);
		frame.getContentPane().add(lblBaseMenor);
		
		lado = new JTextField();
		lado.setText("0");
		lado.setColumns(10);
		lado.setBounds(324, 162, 86, 20);
		frame.getContentPane().add(lado);
		
		base = new JTextField();
		base.setText("0");
		base.setColumns(10);
		base.setBounds(324, 198, 86, 20);
		frame.getContentPane().add(base);
		
		altura = new JTextField();
		altura.setText("0");
		altura.setColumns(10);
		altura.setBounds(324, 229, 86, 20);
		frame.getContentPane().add(altura);
		
		diagonalMayor = new JTextField();
		diagonalMayor.setText("0");
		diagonalMayor.setColumns(10);
		diagonalMayor.setBounds(324, 261, 86, 20);
		frame.getContentPane().add(diagonalMayor);
		
		diagonalMenor = new JTextField();
		diagonalMenor.setText("0");
		diagonalMenor.setColumns(10);
		diagonalMenor.setBounds(324, 292, 86, 20);
		frame.getContentPane().add(diagonalMenor);
		
		baseMayor = new JTextField();
		baseMayor.setText("0");
		baseMayor.setColumns(10);
		baseMayor.setBounds(324, 323, 86, 20);
		frame.getContentPane().add(baseMayor);
		
		baseMenor = new JTextField();
		baseMenor.setText("0");
		baseMenor.setColumns(10);
		baseMenor.setBounds(324, 354, 86, 20);
		frame.getContentPane().add(baseMenor);
		
		JLabel lblResultado = new JLabel("RESULTADO:");
		lblResultado.setBounds(184, 430, 133, 30);
		frame.getContentPane().add(lblResultado);
		
		resultado = new JTextField();
		resultado.setColumns(10);
		resultado.setBounds(324, 435, 86, 20);
		frame.getContentPane().add(resultado);
		
		//btnNewButton = new JButton("New button");
		

		
		hacer = new JButton("Hacer");
		hacer.setBounds(324, 401, 89, 23);
		frame.getContentPane().add(hacer);
		hacer.addActionListener (new ActionListener(){
			public void actionPerformed(ActionEvent e){
			calcularArea(); } }
			);
		
		lblNewLabel = new JLabel("Deber\u00E1 editar el campo de la figura que necesite y el resto dejarlo con 0");
		lblNewLabel.setBounds(60, 106, 540, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JButton salir = new JButton("SALIR");
		salir.setBounds(289, 471, 167, 30);
		frame.getContentPane().add(salir);
		salir.addActionListener (new ActionListener(){
			public void actionPerformed(ActionEvent e){
			System.exit(0);	}
			}
		);
		
	}
	
	public void calcularArea() {
		
		double Resultado;
		
		String Lado = lado.getText(); 
		double dlado = Double.parseDouble (Lado);
		
		String Radio = radio.getText(); 
		double dradio = Double.parseDouble (Radio);
		
		String Base = base.getText(); 
		double dbase = Double.parseDouble (Base);
		
		String Altura = altura.getText(); 
		double daltura = Double.parseDouble (Altura);
		
		String DiagonalMayor = diagonalMayor.getText(); 
		double ddiagonalMayor = Double.parseDouble (DiagonalMayor);
		
		String DiagonalMenor = diagonalMenor.getText(); 
		double ddiagonalMenor = Double.parseDouble (DiagonalMenor);
		
		String BaseMayor = baseMayor.getText(); 
		double dbaseMayor = Double.parseDouble (BaseMayor);
		
		String BaseMenor = baseMenor.getText(); 
		double dbaseMenor = Double.parseDouble (BaseMenor);
		
		String Figura = figura.getSelectedItem().toString();
		
		if(Figura.equals("Circulo")) {
			Resultado=dradio*dradio*3.14;
			resultado.setText(Double.toString(Resultado));
		}
		else if(Figura.equals("Cuadrado")) {
			Resultado=dlado*dlado;
			resultado.setText(Double.toString(Resultado));
		}
		else if(Figura.equals("Rectangulo")) {
			Resultado=dbase*daltura;
			resultado.setText(Double.toString(Resultado));
		}
		else if(Figura.equals("Rombo")) {
			Resultado=(ddiagonalMayor*ddiagonalMenor)/2;
		resultado.setText(Double.toString(Resultado));
		}
		else if(Figura.equals("Trapecio")) {
			Resultado=((dbaseMayor+dbaseMenor)/2)*2;
			resultado.setText(Double.toString(Resultado));
		}
		else if(Figura.equals("Triangulo")) {
			Resultado=(dbase*daltura)/2;
			resultado.setText(Double.toString(Resultado));
		}
	}
}







