import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class VAMOS_2 {

	private JFrame CALCULADORA;
	private JTextField numero1;
	private JTextField numero2;
	private JTextField operacion;
	private JTextField resultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VAMOS_2 window = new VAMOS_2();
					window.CALCULADORA.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VAMOS_2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		CALCULADORA = new JFrame();
		CALCULADORA.setResizable(false);
		CALCULADORA.setBounds(100, 100, 603, 400);
		CALCULADORA.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblOperaciones = new JLabel("CALCULADORA");
		
		numero1 = new JTextField();
		numero1.setColumns(10);
		
		numero2 = new JTextField();
		numero2.setColumns(10);
		
		JLabel lblNumero = new JLabel("Numero 1:");
		
		JLabel lblNumero_1 = new JLabel("Numero 2:");
		
		JButton hacer = new JButton("Hacer");
		hacer.addActionListener (new ActionListener(){
			public void actionPerformed(ActionEvent e){
			calculos(); } }
			);
		
		operacion = new JTextField();
		operacion.setColumns(10);
		
		JLabel lblOperacion = new JLabel("Operacion:");
		
		JTextPane txtpnSuma = new JTextPane();
		txtpnSuma.setText("Suma: 1 \r\nResta: 2\r\nMultiplicacion: 3\r\nDivision: 4");
		
		resultado = new JTextField();
		resultado.setColumns(10);
		
		GroupLayout groupLayout = new GroupLayout(CALCULADORA.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(70)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNumero_1, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblOperaciones, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(lblNumero, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblOperacion, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(numero2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(numero1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(operacion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(137)
							.addComponent(txtpnSuma, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(252)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(resultado, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(hacer))))
					.addContainerGap(225, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(78)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblOperaciones)
						.addComponent(lblNumero)
						.addComponent(numero1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNumero_1)
						.addComponent(numero2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(26)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(operacion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblOperacion))
					.addGap(7)
					.addComponent(txtpnSuma, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(1)
					.addComponent(hacer)
					.addGap(18)
					.addComponent(resultado, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(49, Short.MAX_VALUE))
		);
		CALCULADORA.getContentPane().setLayout(groupLayout);
	}
	
	public void calculos () {
		
		double Resultado;
		
		String Numero1 = numero1.getText(); 
		double Num1 = Double.parseDouble (Numero1);
		
		String Numero2 = numero2.getText(); 
		double Num2 = Double.parseDouble(Numero2);
		
		String Operacion = operacion.getText();
		int oper = Integer.parseInt (Operacion);
		
		if (oper == 1) {
			Resultado=Num1+Num2;
			System.out.println(Num1+Num2);
			resultado.setText(Double.toString(Resultado));
		}
		if (oper == 2) {
			Resultado=Num1-Num2;
			System.out.println(Num1-Num2);
			resultado.setText(Double.toString(Resultado));	
		}
		if (oper == 3) {
			Resultado=Num1*Num2;
			System.out.println(Num1*Num2);
			resultado.setText(Double.toString(Resultado));	
		}
		if (oper == 4) {
			Resultado=Num1/Num2;
			System.out.println(Num1/Num2);
			resultado.setText(Double.toString(Resultado));	
		}
	}
}
