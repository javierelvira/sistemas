
public class equipo {

	String nombre;
	double EdadMediaJugadores;
	char tipo; // 'A' asociacion, 'C' club, 'S' sociedad
	int edad;
	boolean ascenso;
	
	public equipo(String nombre, double edadMediaJugadores, char tipo, int edad, boolean ascenso) {
		super();
		this.nombre = nombre;
		EdadMediaJugadores = edadMediaJugadores;
		this.tipo = tipo;
		this.edad = edad;
		this.ascenso = ascenso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getEdadMediaJugadores() {
		return EdadMediaJugadores;
	}

	public void setEdadMediaJugadores(double edadMediaJugadores) {
		EdadMediaJugadores = edadMediaJugadores;
	}

	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public boolean isAscenso() {
		return ascenso;
	}

	public void setAscenso(boolean ascenso) {
		this.ascenso = ascenso;
	}

	@Override
	public String toString() {
		return "equipo [nombre=" + nombre + ", EdadMediaJugadores=" + EdadMediaJugadores + ", tipo=" + tipo + ", edad="
				+ edad + ", ascenso=" + ascenso + "]";
	}
	
	
	
}
