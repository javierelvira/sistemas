
public class caballo extends animal{

	double altura;
	boolean esSalvaje;
	char sexo; //'M' macho 'H' hembra
	
	public caballo(String nombre, int edad, boolean chip, double peso, String color, double altura, boolean esSalvaje,
			char sexo) {
		super(nombre, edad, chip, peso, color);
		this.altura = altura;
		this.esSalvaje = esSalvaje;
		this.sexo = sexo;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public boolean isEsSalvaje() {
		return esSalvaje;
	}

	public void setEsSalvaje(boolean esSalvaje) {
		this.esSalvaje = esSalvaje;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	@Override
	public String toString() {
		return "caballo [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", altura=" + altura + ", esSalvaje=" + esSalvaje + ", sexo=" + sexo + "]";
	}

	
}
