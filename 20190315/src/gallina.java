
public class gallina extends animal{

	int numHuevos;
	boolean tieneCresta;
	
	public gallina(String nombre, int edad, boolean chip, double peso, String color, int numHuevos,
			boolean tieneCresta) {
		super(nombre, edad, chip, peso, color);
		this.numHuevos = numHuevos;
		this.tieneCresta = tieneCresta;
	}

	public int getNumHuevos() {
		return numHuevos;
	}

	public void setNumHuevos(int numHuevos) {
		this.numHuevos = numHuevos;
	}

	public boolean isTieneCresta() {
		return tieneCresta;
	}

	public void setTieneCresta(boolean tieneCresta) {
		this.tieneCresta = tieneCresta;
	}

	@Override
	public String toString() {
		return "gallina [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", numHuevos=" + numHuevos + ", tieneCresta=" + tieneCresta + "]";
	}
	
	
}
