
public class rombo extends figura{

	double dMayor;
	double dMenor;
	
	public rombo(String color, char tama�o, double dMayor, double dMenor) {
		super(color, tama�o);
		this.dMayor = dMayor;
		this.dMenor = dMenor;
	}

	public double getdMayor() {
		return dMayor;
	}

	public void setdMayor(double dMayor) {
		this.dMayor = dMayor;
	}

	public double getdMenor() {
		return dMenor;
	}

	public void setdMenor(double dMenor) {
		this.dMenor = dMenor;
	}

	@Override
	public String toString() {
		return "rombo [color=" + color + ", tama�o=" + tama�o + ", dMayor=" + dMayor + ", dMenor=" + dMenor + "]";
	}
	public double calcularArea() {
		double area;
		area=(dMayor*dMenor)/2;
		return area;

	}	
}
