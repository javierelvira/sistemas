
public class conejo extends animal{

	int tama�o;
	boolean pelo;
	boolean campero;
	
	public conejo(String nombre, int edad, boolean chip, double peso, String color, int tama�o, boolean pelo,
			boolean campero) {
		super(nombre, edad, chip, peso, color);
		this.tama�o = tama�o;
		this.pelo = pelo;
		this.campero = campero;
	}

	public int getTama�o() {
		return tama�o;
	}

	public void setTama�o(int tama�o) {
		this.tama�o = tama�o;
	}

	public boolean isPelo() {
		return pelo;
	}

	public void setPelo(boolean pelo) {
		this.pelo = pelo;
	}

	public boolean isCampero() {
		return campero;
	}

	public void setCampero(boolean campero) {
		this.campero = campero;
	}

	@Override
	public String toString() {
		return "conejo [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", tama�o=" + tama�o + ", pelo=" + pelo + ", campero=" + campero + "]";
	}
	
	
}
