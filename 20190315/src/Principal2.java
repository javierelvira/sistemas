
public class Principal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		coche coche1 = new coche("1234CVH", "Volvo", "S60", 25000, 'D', 123, 5, true);
		coche coche2 = new coche("1235CVJ", "Mercedes", "CLA", 50000, 'G', 145, 5, true);
		coche coche3 = new coche("1236CVK", "Kia", "Sportage", 20000, 'G', 104, 4, false);
		coche coche4 = new coche("1237CVL", "Opel", "Astra", 26000, 'H', 321, 5, false);
		coche coche5 = new coche("1238CVM", "Volvo", "FH16", 100000, 'D', 840, 6, true);
		coche coche6 = new coche("1239CVN", "Citroen", "Berlingo", 13000, 'G', 631, 5, false);
		coche coche7 = new coche("1240CVP", "Suzuki", "Ninja", 48000, 'G', 550, 2, false);
		coche coche8 = new coche("1241CVQ", "Renault", "Clio", 16500, 'D', 215, 4, false);

		coche[][] arrayDeCoches = new coche[2][4];
		arrayDeCoches[0][0] = coche3;
		arrayDeCoches[0][1] = coche5;
		arrayDeCoches[0][2] = coche7;
		arrayDeCoches[0][3] = coche2;
		arrayDeCoches[1][0] = coche6;
		arrayDeCoches[1][1] = coche1;
		arrayDeCoches[1][2] = coche4;
		arrayDeCoches[1][3] = coche8;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(arrayDeCoches[i][j].getMarca() + "\t");
			}
			System.out.println();
		}
		
		System.out.println(coche1.CalculoIVA());
		System.out.println(coche1.Categoria());
		coche1.pincharRueda();
		System.out.println(coche1.getRuedas());

	}

}
