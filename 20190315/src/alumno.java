
public class alumno {

	String nombre;
	double notamedia;
	char sexo;
	int edad;
	boolean aprobado;
	
	public alumno(String nombre, double notamedia, char sexo, int edad, boolean aprobado) {
		super();
		this.nombre = nombre;
		this.notamedia = notamedia;
		this.sexo = sexo;
		this.edad = edad;
		this.aprobado = aprobado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getNotamedia() {
		return notamedia;
	}

	public void setNotamedia(double notamedia) {
		this.notamedia = notamedia;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public boolean isAprobado() {
		return aprobado;
	}

	public void setAprobado(boolean aprobado) {
		this.aprobado = aprobado;
	}

	@Override
	public String toString() {
		return "alumno [nombre=" + nombre + ", notamedia=" + notamedia + ", sexo=" + sexo + ", edad=" + edad
				+ ", aprobado=" + aprobado + "]";
	}
	
	
	
}
