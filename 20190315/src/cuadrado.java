
public class cuadrado extends figura{

	double lado;

	public cuadrado(String color, char tama�o, double lado) {
		super(color, tama�o);
		this.lado = lado;
	}

	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}

	@Override
	public String toString() {
		return "cuadrado [color=" + color + ", tama�o=" + tama�o + ", lado=" + lado + "]";
	}
	public double calcularArea() {
		double area;
		area=lado*lado;
		return area;

	}
}	
