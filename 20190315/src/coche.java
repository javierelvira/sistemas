
public class coche extends vehiculo {

	char tipo; // 'G' gasolina, 'D' diesel, 'H' hibrido, 'E' electrico, 'O' otro
	int bastidor;
	int ruedas;
	boolean descapotable;
	
	//Creamos metodo constructor
	public coche(String matricula, String marca, String modelo, double precio, char tipo, int bastidor, int ruedas,
			boolean descapotable) {
		super(matricula, marca, modelo, precio);
		this.tipo = tipo;
		this.bastidor = bastidor;
		this.ruedas = ruedas;
		this.descapotable = descapotable;
	}
	
	//Metodos getters && setters
		public char getTipo() {
			return tipo;
		}
		public void setTipo(char tipo) {
			this.tipo = tipo;
		}
		public int getBastidor() {
			return bastidor;
		}
		public void setBastidor(int bastidor) {
			this.bastidor = bastidor;
		}
		public int getRuedas() {
			return ruedas;
		}
		public void setRuedas(int ruedas) {
			this.ruedas = ruedas;
		}
		public boolean isDescapotable() {
			return descapotable;
		}
		public void setDescapotable(boolean descapotable) {
			this.descapotable = descapotable;
		}

	//metodo ToString para sacar el objeto por pantalla
		@Override
		public String toString() {
			return "coche [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
					+ ", tipo=" + tipo + ", bastidor=" + bastidor + ", ruedas=" + ruedas + ", descapotable=" + descapotable
					+ "]";
		}

	//Calcular el precio del coche con IVA
	
	public double CalculoIVA() {
		double calculo = precio * 1.21;
		return calculo;
	}
	


	public char Categoria () {
		char cat=' ';
		if (tipo=='D') cat='B';
		if (tipo=='G') cat='C';
		else cat='A';
		return cat;
	}
	
	public void pincharRueda () {
		
		ruedas = getRuedas()-1;
		
	}
	
	
}








