
public class rectangulo extends figura{

	double base;
	double altura;
	
	public rectangulo(String color, char tama�o, double base, double altura) {
		super(color, tama�o);
		this.base = base;
		this.altura = altura;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "rectangulo [color=" + color + ", tama�o=" + tama�o + ", base=" + base + ", altura=" + altura + "]";
	}
	public double calcularArea() {
		double area;
		area=base*altura;
		return area;

	}
}
