import java.util.Scanner;

public class Principal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Sacar por pantalla los 10 primeros números enteros
		int x=0;
		int n=2;
		int i=0;
		
		while (x<=10) {
			System.out.println("El valor de x es:"+ x);
			x++;
		}
		
		//Sacar por pantalla los 10 primeros numero enteros pares;
		while (n<=20) {
			System.out.println("El valor de n es:"+ n);
			n=n+2;
		}
		
		//Sacar por pantalla de los 20 primeros números, los multiplos de 7 o de 3
		while (i<=20)  {
			if ( (i%3 == 0) || (i%7 == 0)) {
				System.out.println(i);
			}
			i++;
		}
		
		System.out.println("Menú Calculadora");
		//si elijo + sumar
		//si elijo - restar
		//si elijo * multiplicar
		//si elijo / dividir
		//si elijo @ salir
		
		System.out.println("Elija una opción: ");
		Scanner teclado = new Scanner(System.in);
		char seleccion = teclado.next().charAt(0);
		
		switch (seleccion) {
			case '+': System.out.println("Sumar");
						break;
			case '-': System.out.println("Restar");
						break;
			case '*': System.out.println("Multiplicar");
						break;
			case '/': System.out.println("Dividir");
						break;
			case '@': System.out.println("Bye bye");
						break;
			default: System.out.println("Opción no válida");
		}
		
	}

}






