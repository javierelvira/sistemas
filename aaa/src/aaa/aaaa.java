package aaa;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Point;
import java.awt.FlowLayout;
import javax.swing.JScrollPane;
import java.awt.GridLayout;
import java.awt.CardLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.SpringLayout;
import javax.swing.ScrollPaneConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.SwingConstants;

public class aaaa{

	public JFrame frmMarioBros;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					aaaa window = new aaaa();
					window.frmMarioBros.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public aaaa() {
		init();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void init() {
		
		frmMarioBros = new JFrame();
		frmMarioBros.getContentPane().setEnabled(true);
		frmMarioBros.getContentPane().setVisible(true);
		frmMarioBros.setTitle("Mario Bros");
		frmMarioBros.getContentPane().setLocation(new Point(0, 0));
		frmMarioBros.setLocation(new Point(0, 0));
		frmMarioBros.setBounds(100, 100, 1116, 760);
		frmMarioBros.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMarioBros.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 1100, 721);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		frmMarioBros.getContentPane().add(scrollPane);
		
		JLabel fondo = new JLabel("");
		fondo.setFocusable(false);
		fondo.setHorizontalTextPosition(SwingConstants.LEADING);
		fondo.setIcon(new ImageIcon("C:\\Users\\CFGS\\Desktop\\Wprograms\\personalproyects\\tests\\fondo-Mario.png"));
		scrollPane.setViewportView(fondo);
		
		frmMarioBros.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				int x = fondo.getX();
				int y = fondo.getY();
				
				if ( (KeyEvent.VK_D == e.getKeyCode() || KeyEvent.VK_RIGHT == e.getKeyCode()) && x != 200 ) {
					x = x - 5;
				}
				if ( (KeyEvent.VK_A == e.getKeyCode() || KeyEvent.VK_LEFT == e.getKeyCode()) && x != 0 ) {
					x = x + 5;
				}
				fondo.setLocation(x, y);
			}
		});
	}
}
